﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AwarenessLevel))]
public class AwarenessLevelEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		(target as AwarenessLevel).DeviceAngle = 90f - (target as AwarenessLevel).level * 18;
		EditorUtility.SetDirty(target);
		EditorGUILayout.LabelField("Device Angle", (target as AwarenessLevel).DeviceAngle.ToString());
	}
}
