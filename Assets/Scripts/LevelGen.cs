﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class LevelGen : MonoBehaviour
{
	public Transform floorPrefab;
	public Transform columnPrefab;
	public Transform stairsPrefab;
	public Transform levelHolder;


	public Transform maleDopePrefab;
	public Transform maleNondopePrefab;

	public Transform femaleDopePrefab;
	public Transform femaleNondopePrefab;

	public Transform eastWall;
	public Transform westWall;
	public Transform southWall;
	public Transform northWall;

	public Transform tracks;

	public Transform goalPrefab;

	public GoalManager goalManager;
	MessageSpawner messageSpawner;
	CameraSwitcher cameraSwitcher;
	NPCRef npcRef;
	TrainManager trainManager;

	public new Transform camera;

	public Transform image;
	public Transform imageHolder;

	public Name[] names;

	public AwarenessLevel[] awarenessLevels;

	Transform playerInstance;

	NavMeshSurface regular;
	NavMeshSurface dopes;

	//used to store the columns and stairs
	//bool is whether stairs or not
	Dictionary<Vector3Int, bool> occupied = new Dictionary<Vector3Int, bool>();

	//integar is the floor, first entry in list 
	Dictionary<int, List<Vector2Int>> floorDimensions = new Dictionary<int, List<Vector2Int>>();

	//use this to store each object
	//when making stairs, use this to delete the floors above
	Dictionary<Vector3Int, Transform> items = new Dictionary<Vector3Int, Transform>();

	int ceilingHeight = 13;
	int floorTileSize = 10;

	// Start is called before the first frame update
	void Awake()
	{
		trainManager = GetComponent<TrainManager>();
		cameraSwitcher = GetComponent<CameraSwitcher>();
		messageSpawner = GetComponent<MessageSpawner>();
		npcRef = GetComponent<NPCRef>();
		//can probably delete this
		npcRef.DopeEliminated += OnNPCEliminated;

		//set up the different navmeshes
		NavMeshSurface[] sur = levelHolder.GetComponents<NavMeshSurface>();
		for (int i = 0; i < sur.Length; i++)
		{
			if (sur[i].agentTypeID == 0)
			{
				regular = sur[i];
			}
			else
			{
				dopes = sur[i];
			}
		}
	}

	//can probably delete this function
	protected void OnNPCEliminated(bool noneLeft)
	{
		if (noneLeft)
		{
			//Debug.Log("complete");
		}
	}

	public void OnTrapPlaced(Vector3Int pos, GameObject go, bool isPit)
	{
		if (occupied.ContainsKey(pos))
		{
			return;
		}
		else
		{
			occupied.Add(pos, false);
			SetLayerByRecursion(go.transform);
			//make sure to unsubscribe
			go.GetComponent<MoveTrapWithMouse>().Placed -= OnTrapPlaced;
			//destroy the component
			Destroy(go.GetComponent<MoveTrapWithMouse>());

			go.transform.SetParent(levelHolder);

			//make sure to getrid of the floor tile
			if (isPit)
			{
				Destroy(items[pos].gameObject);
			}

			//rebuild the navmesh
			//this is not actually needed!
			//regular.BuildNavMesh();
		}
	}

	private void SetLayerByRecursion(Transform t)
	{
		//make sure that the trigger cones of the pipe remain on the ignore raycast layer
		if (t.tag != "TiggerCone")
		{
			t.gameObject.layer = LayerMask.NameToLayer("Trap");
		}

		foreach (Transform child in t)
		{
			SetLayerByRecursion(child);
		}
	}

	// Update is called once per frame
	void Update()
	{
		//if (Input.GetKeyDown(KeyCode.Space))
		//{
		//	Generate();
		//}
	}

	public void Generate()
	{
		cameraSwitcher.Reset();
		ClearLevel();
		//levelHolder.gameObject.AddComponent<NavMeshSurface>();

		int numFloors = Random.Range(1,
			Mathf.FloorToInt(Mathf.Sqrt(GameManager.instance.NumberCompleted + 2)));

		int x = -1;
		int z = -1;
		bool longAxisEastWest;


		for (int i = 0; i < numFloors; i++)
		{
			longAxisEastWest = Random.Range(0, 2) == 1;
			Vector2Int offset = new Vector2Int();


			if (longAxisEastWest)
			{
				x = Random.Range(15, 31);
				z = Random.Range(5, 11);
			}
			else
			{
				x = Random.Range(5, 11);
				z = Random.Range(15, 31);
			}

			if (floorDimensions.Count > 0)
			{
				int prevX = floorDimensions[i - 1][1].x;
				int prevY = floorDimensions[i - 1][1].y;

				//bool center = Random.Range(0, 2) == 1;
				////if we center, the exact orientation of each floor does not matter
				//if (center)
				//{
				offset.x = floorDimensions[i - 1][0].x + (prevX - x) / 2;
				offset.y = floorDimensions[i - 1][0].y + (prevY - z) / 2;
				//}
				//else
				//{
				//	if ((longAxisEastWest && prevX > prevY))
				//	{
				//		offset.x = Random.Range(0, prevX + 1);
				//		offset.y = floorDimensions[i - 1][0].y + (prevY - z) / 2;
				//	}
				//	else
				//	{
				//		if (prevX > prevY)
				//		{
				//			offset.x = Random.Range(0, prevX + 1);
				//		}
				//		else
				//		{
				//			offset.y = Random.Range(0, prevY + 1);
				//		}
				//		//offset.x = (prevX - x) / 2;
				//		//offset.y = (prevY - z) / 2;
				//	}
				//}
			}

			GenerateLayer(i, x, z, longAxisEastWest, offset);
			//make sure to add the new dimenstions to the array
			List<Vector2Int> floorStartOff = new List<Vector2Int>();
			floorStartOff.Add(offset);
			floorStartOff.Add(new Vector2Int(x, z));
			floorDimensions.Add(i, floorStartOff);
		}

		AddStairs();
		AddColumns();
		AddGoals();


		AddWalls();
		AddTracks();

		AddCameras();

		regular.BuildNavMesh();
		dopes.BuildNavMesh();


		AddNPCs();

		trainManager.StartTrains();
	}

	void AddNPCs()
	{
		int numDopes = Random.Range(1,
			Mathf.FloorToInt(2 * Mathf.Sqrt(GameManager.instance.NumberCompleted + 2)));

		for (int i = 0; i < numDopes; i++)
		{
			Name n = names[Random.Range(0, names.Length)];

			Transform t = goalManager.Goals[Random.Range(0, goalManager.Goals.Count)];
			while (!goalManager.SetNPCAtGoal(t))
			{
				t = goalManager.Goals[Random.Range(0, goalManager.Goals.Count)];
			}

			//NavMeshHit hit;

			//NavMesh.SamplePosition(t.position, out hit, 500, 1);

			switch (n.gender)
			{
				case Gender.Male:
					playerInstance = Instantiate(maleDopePrefab, t.position, Quaternion.identity);
					break;
				case Gender.Female:
					playerInstance = Instantiate(femaleDopePrefab, t.position, Quaternion.identity);
					break;
				default:
					break;
			}
			
			playerInstance.transform.SetParent(levelHolder);
			playerInstance.GetComponent<NPCHolder>().npcName = n;

			//figure out a better way to adjust the difficulty
			int l = 0;
			playerInstance.GetComponent<NPCHolder>().level = awarenessLevels[l];

		

			//playerInstance.GetComponent<NavMeshAgent>().Warp(t.position);

			//set the anim
			//Instantiate(awarenessLevels[l].prefab, playerInstance);
			playerInstance.GetComponent<NPCHolder>().SetAnim(playerInstance.GetComponentInChildren<Animator>());

			//get device holder and activate one of the devices
			bool isTablet = Random.Range(0, 2) == 1;
			if (isTablet)
			{
				playerInstance.GetComponentInChildren<DeviceHolder>().tablet.SetActive(true);
			}
			else
			{
				playerInstance.GetComponentInChildren<DeviceHolder>().cell.SetActive(true);
			}
			 

			messageSpawner.RegisterNPC(playerInstance.GetComponent<NPCHolder>());
			goalManager.RegisterNPC(playerInstance.GetComponent<NPCHolder>());
			NPCRef.instance.AddNPC(playerInstance);
		}

		int numNonDopes = Random.Range(1,
			Mathf.FloorToInt(4 * Mathf.Sqrt(GameManager.instance.NumberCompleted + 2)));
		for (int i = 0; i < numNonDopes; i++)
		{
			Name n = names[Random.Range(0, names.Length)];

			Transform t = goalManager.Goals[Random.Range(0, goalManager.Goals.Count)];
			while (!goalManager.SetNPCAtGoal(t))
			{
				t = goalManager.Goals[Random.Range(0, goalManager.Goals.Count)];
			}

			//NavMeshHit hit;

			//NavMesh.SamplePosition(t.position, out hit, 500, 0);


			switch (n.gender)
			{
				case Gender.Male:
					playerInstance = Instantiate(maleNondopePrefab, t.position, Quaternion.identity);
					break;
				case Gender.Female:
					playerInstance = Instantiate(femaleNondopePrefab, t.position, Quaternion.identity);
					break;
				default:
					break;
			}
			playerInstance.transform.SetParent(levelHolder);
			playerInstance.GetComponent<NPCHolder>().npcName = n;

			
			//playerInstance.GetComponent<NavMeshAgent>().Warp(t.position);

			//set the anim!
			playerInstance.GetComponent<NPCHolder>().SetAnim(playerInstance.GetComponentInChildren<Animator>());

			goalManager.RegisterNPC(playerInstance.GetComponent<NPCHolder>());
		}
	}

	void AddCameras()
	{

		for (int i = 0; i < floorDimensions.Count; i++)
		{
			int mod = 0;
			if (floorDimensions[i][1].x > floorDimensions[i][1].y)
			{
				mod = 1;
			}

			Transform eastCamera = Instantiate(camera, levelHolder);
			eastCamera.name = "Floor " + (1 + i) + " East Camera";
			eastCamera.position = new Vector3((floorDimensions[i][0].x + floorDimensions[i][1].x - mod) * floorTileSize,
				((i + 1) * ceilingHeight) - 4, (floorDimensions[i][0].y + floorDimensions[i][1].y / 2) * floorTileSize);
			eastCamera.rotation = Quaternion.Euler(30, -90, 0f);

			Transform ime = Instantiate(image, imageHolder);
			RenderTexture e = new RenderTexture(1024, 768, 16, RenderTextureFormat.ARGB32);
			e.Create();
			eastCamera.GetComponent<Camera>().targetTexture = e;
			ime.GetComponent<RawImage>().texture = e;

			cameraSwitcher.RegisterCameraView(ime.GetComponent<MakeMainCameraOnClick>());
			ime.GetComponent<MakeMainCameraOnClick>().SetCamera(eastCamera.GetComponent<Camera>());
			ime.GetComponent<MakeMainCameraOnClick>().message = eastCamera.name;
			messageSpawner.RegisterCamera(eastCamera.GetComponent<NPCVisibility>());

			Transform westCamera = Instantiate(camera, levelHolder);
			westCamera.name = "Floor " + (1 + i) + " West Camera";
			westCamera.position = new Vector3(floorDimensions[i][0].x * floorTileSize,
				((i + 1) * ceilingHeight) - 4, (floorDimensions[i][0].y + floorDimensions[i][1].y / 2) * floorTileSize);
			westCamera.rotation = Quaternion.Euler(30, 90, 0f);

			Transform imw = Instantiate(image, imageHolder);
			RenderTexture w = new RenderTexture(1024, 768, 16, RenderTextureFormat.ARGB32);
			w.Create();
			westCamera.GetComponent<Camera>().targetTexture = w;
			imw.GetComponent<RawImage>().texture = w;

			cameraSwitcher.RegisterCameraView(imw.GetComponent<MakeMainCameraOnClick>());
			imw.GetComponent<MakeMainCameraOnClick>().SetCamera(westCamera.GetComponent<Camera>());
			imw.GetComponent<MakeMainCameraOnClick>().message = westCamera.name;
			messageSpawner.RegisterCamera(westCamera.GetComponent<NPCVisibility>());


			Transform southCamera = Instantiate(camera, levelHolder);
			southCamera.name = "Floor " + (1 + i) + " South Camera";
			southCamera.position = new Vector3((floorDimensions[i][0].x + floorDimensions[i][1].x / 2) * floorTileSize,
				((i + 1) * ceilingHeight) - 4, (floorDimensions[i][0].y + floorDimensions[i][1].y - 1 + mod) * floorTileSize);
			southCamera.rotation = Quaternion.Euler(30f, 180f, 0f);

			Transform ims = Instantiate(image, imageHolder);
			RenderTexture s = new RenderTexture(1024, 768, 16, RenderTextureFormat.ARGB32);
			s.Create();
			southCamera.GetComponent<Camera>().targetTexture = s;
			ims.GetComponent<RawImage>().texture = s;

			cameraSwitcher.RegisterCameraView(ims.GetComponent<MakeMainCameraOnClick>());
			ims.GetComponent<MakeMainCameraOnClick>().SetCamera(southCamera.GetComponent<Camera>());
			ims.GetComponent<MakeMainCameraOnClick>().message = southCamera.name;
			messageSpawner.RegisterCamera(southCamera.GetComponent<NPCVisibility>());

			Transform northCamera = Instantiate(camera, levelHolder);
			northCamera.name = "Floor " + (1 + i) + " North Camera";
			northCamera.position = new Vector3((floorDimensions[i][0].x + floorDimensions[i][1].x / 2) * floorTileSize,
				((i + 1) * ceilingHeight) - 4, floorDimensions[i][0].y * floorTileSize);
			northCamera.rotation = Quaternion.Euler(30f, 0f, 0f);

			Transform imn = Instantiate(image, imageHolder);
			RenderTexture n = new RenderTexture(1024, 768, 16, RenderTextureFormat.ARGB32);
			n.Create();
			northCamera.GetComponent<Camera>().targetTexture = n;
			imn.GetComponent<RawImage>().texture = n;

			cameraSwitcher.RegisterCameraView(imn.GetComponent<MakeMainCameraOnClick>());
			imn.GetComponent<MakeMainCameraOnClick>().SetCamera(northCamera.GetComponent<Camera>());
			imn.GetComponent<MakeMainCameraOnClick>().message = northCamera.name;
			messageSpawner.RegisterCamera(northCamera.GetComponent<NPCVisibility>());

			messageSpawner.SetCamera(northCamera.GetComponent<Camera>());

			cameraSwitcher.AddCamera(southCamera.GetComponent<Camera>(), ims.GetComponent<RawImage>());
			cameraSwitcher.AddCamera(eastCamera.GetComponent<Camera>(), ime.GetComponent<RawImage>());
			cameraSwitcher.AddCamera(westCamera.GetComponent<Camera>(), imw.GetComponent<RawImage>());
			cameraSwitcher.AddCamera(northCamera.GetComponent<Camera>(), imn.GetComponent<RawImage>());
		}
	}

	void GenerateLayer(int layer, int x, int z, bool longAxisEastWest, Vector2Int offset)
	{


		//Debug.Log("x:" + x);
		//Debug.Log("z:" + z);

		for (int ew = 0; ew < x; ew++)
		{
			for (int ns = 0; ns < z; ns++)
			{
				//if layer is greater than 0, make sure to check the occupied dictionary
				GameObject go = Instantiate(floorPrefab.gameObject, levelHolder);
				Vector3Int pos = new Vector3Int((ew + offset.x) * floorTileSize, layer * ceilingHeight,
					(ns + offset.y) * floorTileSize);
				go.transform.position = pos;

				items.Add(pos, go.transform);
			}
		}
	}


	void AddStairs()
	{
		if (floorDimensions.Count > 1)
		{
			//Debug.Log("can add stairs");
			//only add stairs going from bottom level to the one above
			//the top level gets no stairs going up
			for (int i = 0; i < floorDimensions.Count - 1; i++)
			{

				int west = Mathf.Max(floorDimensions[i][0].x, floorDimensions[i + 1][0].x);
				int east = Mathf.Min(floorDimensions[i][0].x + floorDimensions[i][1].x,
					floorDimensions[i + 1][0].x + floorDimensions[i + 1][1].x);

				int south = Mathf.Max(floorDimensions[i][0].y, floorDimensions[i + 1][0].y);
				int north = Mathf.Min(floorDimensions[i][0].y + floorDimensions[i][1].y,
					floorDimensions[i + 1][0].y + floorDimensions[i + 1][1].y);

				//Debug.DrawLine(new Vector3(west * floorTileSize -floorTileSize/2,
				//	i * ceilingHeight,south * floorTileSize - floorTileSize / 2),
				//	new Vector3(west * floorTileSize - floorTileSize / 2,
				//	i * ceilingHeight, north * floorTileSize - floorTileSize / 2), Color.black, 10f);

				//Debug.DrawLine(new Vector3(west * floorTileSize - floorTileSize / 2,
				//	i * ceilingHeight, north * floorTileSize - floorTileSize / 2),
				//	new Vector3(east * floorTileSize - floorTileSize / 2,
				//	i * ceilingHeight, north * floorTileSize - floorTileSize / 2), Color.black, 10f);

				//Debug.DrawLine(new Vector3(east * floorTileSize - floorTileSize / 2,
				//	i * ceilingHeight, north * floorTileSize - floorTileSize / 2),
				//	new Vector3(east * floorTileSize - floorTileSize / 2, i * ceilingHeight,
				//	south * floorTileSize - floorTileSize / 2), Color.black, 10f);

				//Debug.DrawLine(new Vector3(east * floorTileSize - floorTileSize / 2,
				//	i * ceilingHeight, south * floorTileSize - floorTileSize / 2),
				//	new Vector3(west * floorTileSize - floorTileSize / 2,
				//	i * ceilingHeight, south * floorTileSize - floorTileSize / 2), Color.black, 10f);

				//the floor has a long east-west dimension
				//we need to rotate the stairs
				bool isEastWest = floorDimensions[i][1].x > floorDimensions[i][1].y;
				Vector3Int pos = GetPosition(i, east, west, south, north,
					isEastWest);

				//spawn only one set of stairs for now
				//make sure to check that stairs don't overlap

				GameObject stairs = Instantiate(stairsPrefab.gameObject, levelHolder);
				stairs.transform.position = pos;
				stairs.transform.rotation = isEastWest ? Quaternion.Euler(0f, 90f, 0f) :
					Quaternion.identity;
				// make sure to add the spots to occupied
				occupied.Add(pos, true);
				Vector3Int posTwo, posThree;
				if (isEastWest)
				{
					posTwo = new Vector3Int(pos.x + floorTileSize, pos.y, pos.z);
					posThree = new Vector3Int(pos.x + 2 * floorTileSize, pos.y, pos.x);
				}
				else
				{
					posTwo = new Vector3Int(pos.x, pos.y, pos.z + floorTileSize);
					posThree = new Vector3Int(pos.x, pos.y, pos.z + 2 * floorTileSize);
				}
				occupied.Add(posTwo, true);
				occupied.Add(posThree, true);

				//remove the floor at pos and posTwo, on the floor above
				pos.y += ceilingHeight;
				Destroy(items[pos].gameObject);
				posTwo.y += ceilingHeight;
				Destroy(items[posTwo].gameObject);

				//mark pos and posTwo as occupied to prevent spawning columns there
				occupied.Add(pos, true);
				occupied.Add(posTwo, true);

				items.Remove(pos);
				items.Remove(posTwo);
			}
		}
	}

	Vector3Int GetPosition(int i, int east, int west, int south, int north, bool isEastWest)
	{
		int xPos, yPos, zPos;
		yPos = i * ceilingHeight;
		Vector3Int posTwo, posThree;
		if (isEastWest)
		{

			//Debug.Log("east-west");
			xPos = Random.Range(west + 1, east - 3) * floorTileSize;
			zPos = Random.Range(south, north) * floorTileSize;
			posTwo = new Vector3Int(xPos + floorTileSize, yPos, zPos);
			posThree = new Vector3Int(xPos + (2 * floorTileSize), yPos, zPos);
			//rot = new Vector3(0f, 90f, 0f);
			//Debug.Log("new x: " + xPos);
			//Debug.Log("new z: " + zPos);
			while (occupied.ContainsKey(new Vector3Int(xPos, yPos, zPos)) ||
			occupied.ContainsKey(posTwo) || occupied.ContainsKey(posThree))
			{
				//we should pick another spot
				xPos = Random.Range(west + 1, east - 3) * floorTileSize;
				zPos = Random.Range(south, north) * floorTileSize;
				posTwo = new Vector3Int(xPos + floorTileSize, yPos, zPos);
				posThree = new Vector3Int(xPos + (2 * floorTileSize), yPos, zPos);
			}
		}
		else
		{
			//Debug.Log("north-south");
			xPos = Random.Range(west, east) * floorTileSize;
			//minus 2 prevents going over the edge
			zPos = Random.Range(south + 1, north - 3) * floorTileSize;
			posTwo = new Vector3Int(xPos, yPos, zPos + floorTileSize);
			posThree = new Vector3Int(xPos, yPos, zPos + 2 * floorTileSize);
			//Debug.Log("new x: " + xPos);
			//Debug.Log("new z: " + zPos);
			//rot = Vector3.zero;
			while (occupied.ContainsKey(new Vector3Int(xPos, yPos, zPos)) ||
			occupied.ContainsKey(posTwo) || occupied.ContainsKey(posThree))
			{
				//we should pick another spot
				xPos = Random.Range(west, east) * floorTileSize;
				//minus 2 prevents going over the edge
				zPos = Random.Range(south + 1, north - 3) * floorTileSize;
				posTwo = new Vector3Int(xPos, yPos, zPos + floorTileSize);
				posThree = new Vector3Int(xPos, yPos, zPos + 2 * floorTileSize);
			}
		}

		return new Vector3Int(xPos, yPos, zPos);
	}

	void AddTracks()
	{
		for (int i = 0; i < floorDimensions.Count; i++)
		{
			Vector3Int firstOne = Vector3Int.zero;
			Vector3Int lastOne = Vector3Int.zero;
			Vector3Int firstTwo = Vector3Int.zero;
			Vector3Int lastTwo = Vector3Int.zero;
			//only add along the long axis
			if (floorDimensions[i][1].x > floorDimensions[i][1].y)
			{

				//east-west is longer than north-south
				for (int x = 0; x < floorDimensions[i][1].x; x++)
				{
					GameObject track = Instantiate(tracks.gameObject, levelHolder);
					track.transform.position = new Vector3(
						(floorDimensions[i][0].x + x) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y + floorDimensions[i][1].y) * floorTileSize);
					track.transform.rotation = Quaternion.Euler(0, 90, 0);

					GameObject trackTwo = Instantiate(tracks.gameObject, levelHolder);
					trackTwo.transform.position = new Vector3(
						(floorDimensions[i][0].x + x) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y - 1) * floorTileSize);
					trackTwo.transform.rotation = Quaternion.Euler(0, 90, 0);

					if (x == 0)
					{
						firstOne = new Vector3Int(
						(floorDimensions[i][0].x + x) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y + floorDimensions[i][1].y) * floorTileSize);

						lastTwo = new Vector3Int(
						(floorDimensions[i][0].x + x) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y - 1) * floorTileSize);
					}

					if (x == floorDimensions[i][1].x - 1)
					{
						lastOne = new Vector3Int(
						(floorDimensions[i][0].x + x) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y + floorDimensions[i][1].y) * floorTileSize);

						firstTwo = new Vector3Int(
						(floorDimensions[i][0].x + x) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y - 1) * floorTileSize);

					}
				}
			}
			else
			{
				//north-south is longer than east-west 
				for (int z = 0; z < floorDimensions[i][1].y; z++)
				{
					GameObject track = Instantiate(tracks.gameObject, levelHolder);
					track.transform.position = new Vector3(
						(floorDimensions[i][0].x + floorDimensions[i][1].x) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y + z) * floorTileSize);

					GameObject trackTwo = Instantiate(tracks.gameObject, levelHolder);
					trackTwo.transform.position = new Vector3(
						(floorDimensions[i][0].x - 1) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y + z) * floorTileSize);

					if (z == 0)
					{
						firstOne = new Vector3Int(
						(floorDimensions[i][0].x + floorDimensions[i][1].x) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y + z) * floorTileSize);


						lastTwo = new Vector3Int(
						(floorDimensions[i][0].x - 1) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y + z) * floorTileSize);
					}

					if (z == floorDimensions[i][1].y - 1)
					{
						lastOne = new Vector3Int(
						(floorDimensions[i][0].x + floorDimensions[i][1].x) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y + z) * floorTileSize);

						firstTwo = new Vector3Int(
						(floorDimensions[i][0].x - 1) * floorTileSize,
						i * ceilingHeight,
						(floorDimensions[i][0].y + z) * floorTileSize);
					}
				}
			}

			trainManager.AddTrainTrack(firstOne, lastOne);
			trainManager.AddTrainTrack(firstTwo, lastTwo);
		}
	}
	void AddWalls()
	{
		for (int i = 0; i < floorDimensions.Count; i++)
		{
			int mod = 0;
			if (floorDimensions[i][1].x > floorDimensions[i][1].y)
			{
				mod = 1;
			}

			for (int x = 0; x < floorDimensions[i][1].x; x++)
			{
				if (x == 0)
				{

					//add west walls
					for (int y = 0; y < floorDimensions[i][1].y; y++)
					{
						GameObject wall = Instantiate(westWall.gameObject, levelHolder);
						wall.transform.position = new Vector3(
							(floorDimensions[i][0].x - 1 + mod) * floorTileSize - floorTileSize / 2
							,
							i * ceilingHeight,
							(floorDimensions[i][0].y + y) * floorTileSize);

					}

					GameObject wallSouth = Instantiate(southWall.gameObject, levelHolder);
					wallSouth.transform.position = new Vector3(
						(floorDimensions[i][0].x + x) * floorTileSize
						,
						i * ceilingHeight,
						(floorDimensions[i][0].y - mod) * floorTileSize - floorTileSize / 2);

					GameObject wallNorth = Instantiate(northWall.gameObject, levelHolder);
					wallNorth.transform.position = new Vector3(
						(floorDimensions[i][0].x + x) * floorTileSize
						,
						i * ceilingHeight,
						(floorDimensions[i][0].y + floorDimensions[i][1].y - 1 + mod) * floorTileSize + floorTileSize / 2);
				}
				else if (x == floorDimensions[i][1].x - 1)
				{
					//add east walls
					for (int y = 0; y < floorDimensions[i][1].y; y++)
					{
						GameObject wall = Instantiate(eastWall.gameObject, levelHolder);
						wall.transform.position = new Vector3(
							(floorDimensions[i][0].x + floorDimensions[i][1].x + 1 - mod) * floorTileSize - floorTileSize / 2
							,
							i * ceilingHeight,
							(floorDimensions[i][0].y + y) * floorTileSize);
					}

					GameObject wallSouth = Instantiate(southWall.gameObject, levelHolder);
					wallSouth.transform.position = new Vector3(
						(floorDimensions[i][0].x + x) * floorTileSize
						,
						i * ceilingHeight,
						(floorDimensions[i][0].y - mod) * floorTileSize - floorTileSize / 2);

					GameObject wallNorth = Instantiate(northWall.gameObject, levelHolder);
					wallNorth.transform.position = new Vector3(
						(floorDimensions[i][0].x + x) * floorTileSize
						,
						i * ceilingHeight,
						(floorDimensions[i][0].y + floorDimensions[i][1].y - 1 + mod) * floorTileSize + floorTileSize / 2);
				}
				else
				{

					//add north and south walls
					//use y = floorDimensions[i][0].y and y = floorDimensions[i][0].y + floorDimensions[i][1].y-1
					GameObject wallSouth = Instantiate(southWall.gameObject, levelHolder);
					wallSouth.transform.position = new Vector3(
						(floorDimensions[i][0].x + x) * floorTileSize
						,
						i * ceilingHeight,
						(floorDimensions[i][0].y - mod) * floorTileSize - floorTileSize / 2);

					GameObject wallNorth = Instantiate(northWall.gameObject, levelHolder);
					wallNorth.transform.position = new Vector3(
						(floorDimensions[i][0].x + x) * floorTileSize
						,
						i * ceilingHeight,
						(floorDimensions[i][0].y + floorDimensions[i][1].y - 1 + mod) * floorTileSize + floorTileSize / 2);
				}
			}
		}
	}

	void AddColumns()
	{
		//Debug.Log("can add stairs");
		//only add stairs going from bottom level to the one above
		//the top level gets no stairs going up
		for (int i = 0; i < floorDimensions.Count; i++)
		{

			int num;
			//has a long east west axis
			if (floorDimensions[i][1].x > floorDimensions[i][1].y)
			{
				num = floorDimensions[i][1].x / 5;
				for (int toAdd = 0; toAdd < num; toAdd++)
				{
					int xPos, yPos, zPos;
					yPos = i * ceilingHeight;
					xPos = (floorDimensions[i][0].x + (floorDimensions[i][1].x / 5 * toAdd)) * floorTileSize;
					zPos = Random.Range(floorDimensions[i][0].y + (floorDimensions[i][1].y / 2) - 2
						, floorDimensions[i][0].y + (floorDimensions[i][1].y / 2) + 2) * floorTileSize;


					while (occupied.ContainsKey(new Vector3Int(xPos, yPos, zPos)))
					{
						//we should pick another spot
						zPos = Random.Range(floorDimensions[i][0].y + (floorDimensions[i][1].y / 2) - 2
							, floorDimensions[i][0].y + (floorDimensions[i][1].y / 2) + 2) * floorTileSize;
					}

					//spawn only one set of stairs for now
					//make sure to check that stairs don't overlap
					Vector3Int pos = new Vector3Int(xPos, yPos, zPos);
					GameObject column = Instantiate(columnPrefab.gameObject, levelHolder);
					column.transform.position = pos;
					//make sure to add the spots to occupied
					occupied.Add(pos, false);
				}
			}
			else
			{
				num = floorDimensions[i][1].y / 5;
				for (int toAdd = 0; toAdd < num; toAdd++)
				{
					int xPos, yPos, zPos;
					yPos = i * ceilingHeight;
					xPos = Random.Range(floorDimensions[i][0].x + (floorDimensions[i][1].x / 2) - 1
						, floorDimensions[i][0].x + (floorDimensions[i][1].x / 2) + 1) * floorTileSize;
					zPos = (floorDimensions[i][0].y + (floorDimensions[i][1].y / 5 * toAdd)) * floorTileSize;


					while (occupied.ContainsKey(new Vector3Int(xPos, yPos, zPos)))
					{
						//we should pick another spot
						xPos = Random.Range(floorDimensions[i][0].x + (floorDimensions[i][1].x / 2) - 1
						, floorDimensions[i][0].x + (floorDimensions[i][1].x / 2) + 1) * floorTileSize;
					}

					//spawn only one set of stairs for now
					//make sure to check that stairs don't overlap

					Vector3Int pos = new Vector3Int(xPos, yPos, zPos);
					GameObject column = Instantiate(columnPrefab.gameObject, levelHolder);
					column.transform.position = pos;
					//make sure to add the spots to occupied
					occupied.Add(pos, false);
				}
			}


		}
	}

	void AddGoals()
	{
		for (int i = 0; i < floorDimensions.Count; i++)
		{
			//pick a number of goals per level
			int num = Mathf.FloorToInt(Mathf.Sqrt(floorDimensions[i][1].x * floorDimensions[i][1].y));
			for (int x = 0; x < num; x++)
			{
				int xPos = Random.Range(floorDimensions[i][0].x,
					floorDimensions[i][0].x + floorDimensions[i][1].x);
				int zPos = Random.Range(floorDimensions[i][0].y,
					floorDimensions[i][0].y + floorDimensions[i][1].y);
				int yPos = ceilingHeight * i;

				while (occupied.ContainsKey(new Vector3Int(xPos, yPos, zPos)))
				{
					xPos = Random.Range(floorDimensions[i][0].x, floorDimensions[i][0].x + floorDimensions[i][1].x);
					zPos = Random.Range(floorDimensions[i][0].y, floorDimensions[i][0].y + floorDimensions[i][1].y);
				}

				GameObject goal = Instantiate(goalPrefab.gameObject, levelHolder);
				goal.transform.position = new Vector3Int(xPos * floorTileSize, yPos, zPos * floorTileSize);
				goalManager.AddGoal(goal.transform);
			}
		}
	}

	void ClearLevel()
	{
		for (int i = levelHolder.childCount - 1; i > -1; i--)
		{
			if (levelHolder.GetChild(i).GetComponent<Camera>() != null)
			{
				cameraSwitcher.RemoveCamera(levelHolder.GetChild(i).GetComponent<Camera>());
			}
			Destroy(levelHolder.GetChild(i).gameObject);
		}

		if (playerInstance != null)
		{
			Destroy(playerInstance.gameObject);
		}

		npcRef.NPCs.Clear();

		//for (int i = imageHolder.childCount - 1; i > -1; i--)
		//{
		//	cameraSwitcher.RemoveCamera
		//	Destroy(imageHolder.GetChild(i).gameObject);
		//}


		occupied.Clear();
		floorDimensions.Clear();
		items.Clear();
		//Destroy(levelHolder.GetComponent<NavMeshSurface>().navMeshData);
		//Destroy(levelHolder.GetComponent<NavMeshSurface>());
		//levelHolder.GetComponent<NavMeshSurface>().BuildNavMesh();
	}
}
