﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BodyPartTagHolder")]
public class BodyPartTagHolder : ScriptableObject
{
	public List<string> bodyParts = new List<string>();
}
