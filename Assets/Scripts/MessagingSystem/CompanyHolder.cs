﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CompanyHolder")]
public class CompanyHolder : ScriptableObject
{
	public List<Company> companies = new List<Company>();
}
