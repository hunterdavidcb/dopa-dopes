﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CelebrityTagHolder")]
public class CelebrityTagHolder : ScriptableObject
{
	public List<Name> names = new List<Name>();
}
