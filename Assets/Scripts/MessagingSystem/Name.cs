﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Name")]
public class Name : ScriptableObject
{
	public Gender gender;

	public string GetPronoun(PronounType pt)
	{
		switch (pt)
		{
			case PronounType.SubjectL:
				return gender == Gender.Male ? "he" : "she";
			case PronounType.ObjectL:
				return gender == Gender.Male ? "him" : "her";
			case PronounType.PossessiveL:
				return gender == Gender.Male ? "his" : "her";
			case PronounType.SubjectC:
				return gender == Gender.Male ? "He" : "She";
			//this case is probably not needed
			case PronounType.ObjectC:
				return gender == Gender.Male ? "Him" : "Her";
			case PronounType.PossessiveC:
				return gender == Gender.Male ? "His" : "Her";
		}

		return "";
	}
}

public enum Gender
{
	Male,
	Female
}

public enum PronounType
{
	SubjectC,
	ObjectC,
	PossessiveC,
	SubjectL,
	ObjectL,
	PossessiveL
}
