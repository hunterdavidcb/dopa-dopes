﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MessageHolder : MonoBehaviour
{
	float timer;
	public Text message;
	public Image sns;
	public bool deactivated = false;

	//set up events to message spawner
	public delegate void MessageDestructionHandler(MessageHolder mh);
	public event MessageDestructionHandler MessageDestroyed;

	public void SetLifeTime(float life)
	{
		timer = life;
	}
	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		timer -= Time.deltaTime;
		if (timer <= 0)
		{
			//send destroy message to spawner
			if (MessageDestroyed != null)
			{
				MessageDestroyed(this);
			}
		}
	}

	public void OnNPCPosChanged(Vector3 pos)
	{
		Vector2 temp = MessageSpawner.CurrentCamera.WorldToScreenPoint((pos + Vector3.up * 5f));
		//Vector2 scale = new Vector2(MessageSpawner.RectTransform.rect.width / Screen.width,
		//	MessageSpawner.RectTransform.rect.height / Screen.height);
		transform.position = temp;// -MessageSpawner.RectTransform.anchoredPosition;

		//transform.localScale = new Vector3(scale.x, scale.y, 1f);
	}

	public void SetVisible(bool visible)
	{
		//Debug.Log(visible);
		if (visible != gameObject.activeSelf && !deactivated)
		{
			gameObject.SetActive(visible);
		}
		
	}
}
