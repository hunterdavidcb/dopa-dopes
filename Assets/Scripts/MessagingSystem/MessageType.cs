﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MessageType")]
public class MessageType : ScriptableObject
{
	public string content = "<SENDER>";
	public string response = "<RECEIVER>";
	public Sprite sprite;
}
