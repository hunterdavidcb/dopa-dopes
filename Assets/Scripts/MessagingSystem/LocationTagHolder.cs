﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LocationTagHolder")]
public class LocationTagHolder : ScriptableObject
{
	public List<string> locations = new List<string>();
}
