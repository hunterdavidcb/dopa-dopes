﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SNS")]
public class SNS : ScriptableObject
{
	public Sprite sprite;
}
