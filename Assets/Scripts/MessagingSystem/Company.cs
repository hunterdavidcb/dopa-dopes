﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Company")]
public class Company : ScriptableObject
{
	public string productOrService;
}
