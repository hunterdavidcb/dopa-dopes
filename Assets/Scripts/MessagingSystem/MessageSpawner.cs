﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageSpawner : MonoBehaviour
{
	static RectTransform mainCanvas;
	public GameObject messagePrefab;
	//float timer;
	//float lifeTime;
	//GameObject messageObject;
	public Name[] names;
	public MessageType[] messages;
	public LocationTagHolder location;
	public LocationTagHolder buildings;
	public CompanyHolder companies;
	public BodyPartTagHolder bodyparts;
	public CelebrityTagHolder celebrities;
	public SNS[] sns;
	//bool awaitingResponse = false;
	//int messageIndex = -1;
	//int snsIndex = -1;

	Dictionary<MessageHolder, NPCHolder> npcsMessages = new Dictionary<MessageHolder, NPCHolder>();
	Dictionary<NPCHolder, MessageHolder> messagesNPCs = new Dictionary<NPCHolder, MessageHolder>();
	Dictionary<MessageHolder, bool> awaitingResponses = new Dictionary<MessageHolder, bool>();
	Dictionary<MessageHolder, int> messageIndices = new Dictionary<MessageHolder, int>();
	Dictionary<MessageHolder, int> snsIndices = new Dictionary<MessageHolder, int>();

	//store reference to the currently active camera
	//make the reference static, so we can access it from other scripts
	public static Camera CurrentCamera
	{
		get { return currentCamera; }
	}

	//stores the currently active camera
	static Camera currentCamera;

	//public static RectTransform RectTransform
	//{
	//	get { return mainCanvas; }
	//}

	// Start is called before the first frame update
	void Start()
	{
		mainCanvas = FindObjectOfType<Canvas>().transform.GetComponent<RectTransform>();
		GetComponent<CameraSwitcher>().CameraSwitched += OnCameraSwitched;
	}

	protected void OnCameraSwitched(Camera c)
	{
		currentCamera = c;
		//currentCamera.
		//Camera.main.enabled = false;
	}

	public void RegisterCamera(NPCVisibility nv)
	{
		nv.NPCVisible += OnVisibilityChanged;
	}

	//TODO; this script should be notified when an npc becomes visible/invisible
	// send a message to any messageholders associated with that npc
	protected void OnVisibilityChanged(Transform t, bool isVisible)
	{
		if (t.GetComponent<NPCHolder>() != null)
		{
			NPCHolder npc = t.GetComponent<NPCHolder>();
			if (messagesNPCs.ContainsKey(npc))
			{
				if (messagesNPCs[npc] != null)
				{
					//Debug.Log(isVisible);
					messagesNPCs[npc].SetVisible(isVisible);
				}
			}
			
		}
	}

	public void SetCamera(Camera c)
	{
		currentCamera = c;
	}

	// Update is called once per frame
	void Update()
	{
		

	}

	public void RegisterNPC(NPCHolder npcHolder)
	{
		npcHolder.MessageRequested += OnMessageRequested;
	}

	protected void OnMessageDestroyed(MessageHolder mh)
	{
		if (awaitingResponses[mh])
		{
			mh.deactivated = true;
			mh.gameObject.SetActive(false);
		}
		else
		{
			mh.MessageDestroyed -= OnMessageDestroyed;
			npcsMessages[mh].TransformChanged -= mh.OnNPCPosChanged;
			mh.MessageDestroyed -= npcsMessages[mh].MessageDestroyed;

			messagesNPCs.Remove(npcsMessages[mh]);
			npcsMessages.Remove(mh);

			Destroy(mh.gameObject);
		}

		
	}




	protected void OnMessageRequested(NPCHolder npcHolder)
	{
		if (messagesNPCs.ContainsKey(npcHolder))
		{
			if (awaitingResponses[messagesNPCs[npcHolder]])
			{
				//we are awaiting a response
				string messageText = messages[messageIndices[messagesNPCs[npcHolder]]].response;

				messageText = GetStringFromMessage(messageText, npcHolder);

				messagesNPCs[npcHolder].gameObject.SetActive(true);
				messagesNPCs[npcHolder].message.text = messageText;
				messagesNPCs[npcHolder].SetLifeTime(Random.Range(1, 10f));

				awaitingResponses[messagesNPCs[npcHolder]] = false;
			}
			else
			{
				GameObject messageObject = Instantiate(messagePrefab, mainCanvas.transform);
				//change this to use the currently active camera
				messageObject.transform.position = 
					Camera.main.WorldToScreenPoint(npcHolder.transform.position + Vector3.up * 5f);

				if (!npcsMessages.ContainsKey(messageObject.GetComponent<MessageHolder>()))
				{
					npcsMessages.Add(messageObject.GetComponent<MessageHolder>(), npcHolder);
				}

				messagesNPCs[npcHolder] = messageObject.GetComponent<MessageHolder>();

				awaitingResponses[messagesNPCs[npcHolder]] = true;

				int snsIndex = Random.Range(0, sns.Length);
				snsIndices.Add(messageObject.GetComponent<MessageHolder>(), snsIndex);
				messageObject.GetComponent<MessageHolder>().sns.sprite = sns[snsIndex].sprite;
				int messageIndex = Random.Range(0, messages.Length);
				messageIndices.Add(messageObject.GetComponent<MessageHolder>(), messageIndex);

				string messageText = messages[messageIndex].content;

				messageText = GetStringFromMessage(messageText, npcHolder);

				//messageObject.GetComponentInChildren<Text>().text = "\u0040" + messageText;

				messageObject.GetComponent<MessageHolder>().MessageDestroyed += OnMessageDestroyed;
				npcsMessages[messageObject.GetComponent<MessageHolder>()].TransformChanged
					+= messageObject.GetComponent<MessageHolder>().OnNPCPosChanged;
				messageObject.GetComponent<MessageHolder>().MessageDestroyed
					+= npcsMessages[messageObject.GetComponent<MessageHolder>()].MessageDestroyed;

				messageObject.GetComponent<MessageHolder>().message.text = messageText;

				//make sure to reset the timer for next time
				messageObject.GetComponent<MessageHolder>().SetLifeTime(Random.Range(1, 10f));
			}
		}
		else
		{
			GameObject messageObject = Instantiate(messagePrefab, mainCanvas.transform);

			//change this to use the currently active camera
			messageObject.transform.position =
				Camera.main.WorldToScreenPoint(npcHolder.transform.position + Vector3.up * 5f);

			//TODO
			// make sure to add the messageHolder to the dictionary,
			//or if it already exists, to activate it
			//me
			npcsMessages.Add(messageObject.GetComponent<MessageHolder>(), npcHolder);
			

			messagesNPCs.Add(npcHolder, messageObject.GetComponent<MessageHolder>());

			awaitingResponses[messagesNPCs[npcHolder]] = true;

			int snsIndex = Random.Range(0, sns.Length);
			snsIndices.Add(messageObject.GetComponent<MessageHolder>(), snsIndex);
			messageObject.GetComponent<MessageHolder>().sns.sprite = sns[snsIndex].sprite;
			int messageIndex = Random.Range(0, messages.Length);
			messageIndices.Add(messageObject.GetComponent<MessageHolder>(), messageIndex);

			string messageText = messages[messageIndex].content;
			//Debug.Log(messageText);
			messageText = GetStringFromMessage(messageText, npcHolder);
			

			//messageObject.GetComponentInChildren<Text>().text = "\u0040" + messageText;
			messageObject.GetComponent<MessageHolder>().MessageDestroyed += OnMessageDestroyed;
			npcsMessages[messageObject.GetComponent<MessageHolder>()].TransformChanged
				+= messageObject.GetComponent<MessageHolder>().OnNPCPosChanged;
			messageObject.GetComponent<MessageHolder>().MessageDestroyed
				+= npcsMessages[messageObject.GetComponent<MessageHolder>()].MessageDestroyed;

			messageObject.GetComponent<MessageHolder>().message.text = messageText;

			//make sure to reset the timer for next time
			messageObject.GetComponent<MessageHolder>().SetLifeTime(Random.Range(1, 10f));
		}
	}

	string GetStringFromMessage(string baseMessage, NPCHolder npcHolder)
	{
		baseMessage = baseMessage.Replace("<RECEIVER>", npcHolder.npcName.name + ": ");
		baseMessage = baseMessage.Replace("<SENDER>", names[Random.Range(0, names.Length)].name + ": ");

		Company c = companies.companies[Random.Range(0, companies.companies.Count)];
		baseMessage = baseMessage.Replace("<COMPANY>", c.name);
		baseMessage = baseMessage.Replace("<PRODUCT>", c.productOrService);

		Name celeb = celebrities.names[Random.Range(0, celebrities.names.Count)];
		baseMessage = baseMessage.Replace("<CELEBRITY>", celeb.name);

		baseMessage = baseMessage.Replace("<BODYPART>", bodyparts.bodyParts[Random.Range(0, bodyparts.bodyParts.Count)]);

		baseMessage = baseMessage.Replace("<COUNTRY>", location.locations[Random.Range(0, location.locations.Count)]);

		baseMessage = baseMessage.Replace("<BUILDING>", buildings.locations[Random.Range(0, buildings.locations.Count)]);

		baseMessage = baseMessage.Replace("<SUBJECTC>", celeb.GetPronoun(PronounType.SubjectC));
		baseMessage = baseMessage.Replace("<SUBJECTL>", celeb.GetPronoun(PronounType.SubjectL));


		baseMessage = baseMessage.Replace("<SUBJECTOPPC>", celeb.gender == Gender.Male ? "She" : "He");
		baseMessage = baseMessage.Replace("<SUBJECTOPPL>", celeb.gender == Gender.Male ? "she" : "he");

		baseMessage = baseMessage.Replace("<OBJECTC>", celeb.GetPronoun(PronounType.ObjectC));
		baseMessage = baseMessage.Replace("<OBJECTL>", celeb.GetPronoun(PronounType.ObjectL));

		baseMessage = baseMessage.Replace("<POSSESSIVEC>", celeb.GetPronoun(PronounType.PossessiveC));
		baseMessage = baseMessage.Replace("<POSSESSIVEL>", celeb.GetPronoun(PronounType.PossessiveL));

		return baseMessage;
	}
}
