﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimHook : MonoBehaviour
{
	public delegate void IdleEndHandler(int c);
	public event IdleEndHandler IdleEnded;
	
	[FMODUnity.EventRef]
	public string footstep;


	public void Footstep()
	{
		//Debug.Log("calling");
		//play the footstep here
		FMODUnity.RuntimeManager.PlayOneShot(footstep,transform.position);
	}

	public void IdleEnd()
	{
		if (IdleEnded != null)
		{
			IdleEnded(GetComponent<Animator>().GetInteger("idle"));
		}
	}
}
