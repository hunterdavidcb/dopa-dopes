﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Copyright : MonoBehaviour 
{
    public string t;
	// Use this for initialization
	void Start () 
    {
        GetComponent<Text>().text = "\u00a9" +  t.Replace(@"\n","\n");

    }
	
	// Update is called once per frame
	void Update () 
    {
		
	}
}
