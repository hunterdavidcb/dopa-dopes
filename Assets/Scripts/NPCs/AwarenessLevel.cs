﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AwarenessLevel")]
public class AwarenessLevel : ScriptableObject
{
	public GameObject prefab;
	public float DeviceAngle
	{
		get { return deviceAngle; }
		set { deviceAngle = value; }
	}

	[Range(0,5)]
	public int level;
	[Range(0f,90f)]
	float deviceAngle;
}
