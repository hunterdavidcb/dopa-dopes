﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Wander : MonoBehaviour
{
	NavMeshAgent agent;
	GameObject[] goals;
    // Start is called before the first frame update
    void Start()
    {
		agent = GetComponent<NavMeshAgent>();
		goals = GameObject.FindGameObjectsWithTag("Goal");
		NavMeshHit hit;
		//-1 means everything
		NavMesh.SamplePosition(goals[Random.Range(0, goals.Length)].transform.position, out hit, float.PositiveInfinity, -1);
		agent.SetDestination(hit.position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
