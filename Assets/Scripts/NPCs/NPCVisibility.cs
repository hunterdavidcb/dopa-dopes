﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCVisibility : MonoBehaviour
{
	public delegate void NPCVisibilityHandler(Transform t, bool isVisible);
	public event NPCVisibilityHandler NPCVisible;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		CheckNPCs();
    }

	void CheckNPCs()
	{
		//Debug.Log("checking npcs from " + name);
		for (int n = 0; n < NPCRef.instance.NPCs.Count; n++)
		{
			if (NPCVisible != null)
			{
				NPCVisible(NPCRef.instance.NPCs[n],NPCisVisible(NPCRef.instance.NPCs[n]));
			}
		}
	}



	bool NPCisVisible(Transform t)
	{
		Ray ray = new Ray(transform.position, (t.position - transform.position).normalized);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, float.PositiveInfinity))
		{
			return hit.collider.transform == t;
		}

		
		return false;
	}
}
