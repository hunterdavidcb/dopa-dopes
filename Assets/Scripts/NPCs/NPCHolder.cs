﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCHolder : MonoBehaviour
{
	public Name npcName;
	public AwarenessLevel level;
	NavMeshAgent agent;
	Transform currentGoal;

	public delegate void ArrivalHandler(NPCHolder npc, Transform currentGoal);
	public event ArrivalHandler Arrived;
	bool sent = false;

	public delegate void TransformChangeHandler(Vector3 pos);
	public event TransformChangeHandler TransformChanged;

	public delegate void MessageRequestHandler(NPCHolder npcHolder);
	public event MessageRequestHandler MessageRequested;
	bool requested = false;

	float timerBetweenMessages;

	//float nudgeTimer;
	Animator anim;

	private void Awake()
	{
		agent = GetComponent<NavMeshAgent>();
		//if (agent)
		//{
		//	Debug.Log(name);
		//}
		
		//anim = GetComponentInChildren<Animator>();
		//anim.SetBool("isWaiting", true);
		timerBetweenMessages = UnityEngine.Random.Range(1f, 5f);
		//change this to be influenced by the awarenesslevel
		//if (level != null)
		//{
		//	nudgeTimer = UnityEngine.Random.Range(10f * (level.level + 1), 20f * (level.level + 1));
		//}

		GetComponentInChildren<AnimHook>().IdleEnded += OnIdleEnded;
		
	}

	public void SetAnim(Animator a)
	{
		anim = a;
		anim.SetBool("isWaiting", true);
		if (level != null)
		{
			anim.SetInteger("level", level.level);
		}
		
		anim.GetComponent<AnimHook>().IdleEnded += OnIdleEnded;
		anim.SetFloat("offset", UnityEngine.Random.Range(0f, 0.75f));
	}

	private void OnIdleEnded(int c)
	{
		//Debug.Log("idle finished on " + npcName.name);
		anim.SetInteger("idle", UnityEngine.Random.Range(0, 6));
		
	}

	public void SetGoal(Transform g)
	{
		sent = false;
		currentGoal = g;
		agent.isStopped = false;
		anim.SetBool("isWaiting", false);
		agent.SetDestination(g.position);
	}

	private void Update()
	{
		timerBetweenMessages -= Time.deltaTime;
		//nudgeTimer -= Time.deltaTime;

		//if (nudgeTimer <= 0f && tag == "Dope" && !HasArrived())
		//{
		//	StartCoroutine(GiveNudge());
		//	nudgeTimer = UnityEngine.Random.Range(10f * (level.level + 1), 20f * (level.level + 1));
		//}

		if (timerBetweenMessages <= 0f && !requested)
		{
			//request a message
			if (MessageRequested != null)
			{
				//Debug.Log("requesting");
				MessageRequested(this);
				requested = true;
			}
		}

		if (HasArrived() && !sent)
		{
			sent = true;
			//Debug.Log(name);
			if (anim != null)
			{
				anim.SetBool("isWaiting", true);
				anim.SetInteger("idle", UnityEngine.Random.Range(0, 6));
			}
			
			StartCoroutine(WaitAtGoal());
		}

		if (transform.hasChanged)
		{
			if (TransformChanged != null)
			{
				TransformChanged(transform.position);
			}
		}
	}

	private bool HasArrived()
	{
		if (agent != null && agent.enabled)
		{
			if (!agent.pathPending)
			{
				if (agent.remainingDistance <= agent.stoppingDistance)
				{
					if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
					{
						return true;
					}

					return false;
				}

				return false;

			}
		}
		

		return false;
	}

	public void MessageDestroyed(MessageHolder mh)
	{
		requested = false;
		timerBetweenMessages = UnityEngine.Random.Range(1f, 5f);
	}

	private IEnumerator WaitAtGoal()
	{
		agent.isStopped = true;

		yield return new WaitForSeconds(10f);

		if (anim != null)
		{
			anim.SetBool("isWaiting", false);
		}

		if (Arrived != null)
		{
			Arrived(this, currentGoal);
		}
	}

	//private IEnumerator GiveNudge()
	//{
	//	//convert from world to local space
	//	//Vector2 dir = new Vector2(transform.InverseTransformDirection(transform.right).x,0f);

	//	//change the direction randomly from left to right
	//	//dir *= UnityEngine.Random.Range(0f,1f) > 0.5f? UnityEngine.Random.Range(50f,200f) :
	//	//	UnityEngine.Random.Range(-200f, -50f);
	//	Debug.Log("nudging");
	//	//GetComponent<Rigidbody>().isKinematic = false;
	//	//agent.isStopped = true;
	//	////agent.enabled = false;

	//	////convert back into world space
	//	//GetComponent<Rigidbody>().AddForce(transform.TransformDirection(dir).x, 0f, 0f);
	//	bool isLeft = UnityEngine.Random.Range(0, 2) == 0;
	//	if (isLeft)
	//	{
	//		anim.SetTrigger("nudgeL");
	//	}
	//	else
	//	{
	//		anim.SetTrigger("nudgeR");
	//	}

	//	Vector3 goal = agent.destination;

	//	NavMeshHit hit;
	//	//make sure the position is back on the navmesh
	//	agent.SamplePathPosition(0, 10f, out hit);
	//	Vector3 tempGoal = hit.position;
	//	tempGoal += 0.5f * transform.forward;
	//	tempGoal += isLeft ? - 2f * transform.right : 2f * transform.right;
	//	agent.updateRotation = false;
	//	float orginalSpeed = agent.speed;
	//	agent.SetDestination(tempGoal);
	//	agent.speed = orginalSpeed * 5f;
	//	//agent.enabled = true;


	//	yield return new WaitForSeconds(1f);

		
	//	agent.SetDestination(goal);

	//	agent.speed = orginalSpeed;
	//	agent.updateRotation = true;

	//	//GetComponent<Rigidbody>().isKinematic = true;
	//	//GetComponent<Rigidbody>().velocity = Vector3.zero;
	//	//NavMeshHit hit;
	//	//agent.enabled = true;
	//	//make sure the position is back on the navmesh
	//	//agent.SamplePathPosition(0, 10f, out hit);
	//	//transform.position = hit.position;
	//	//agent.isStopped = false;
	//}
}
