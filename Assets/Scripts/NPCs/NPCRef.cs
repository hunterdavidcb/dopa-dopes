﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCRef : MonoBehaviour
{
	public static NPCRef instance;

	List<Transform> npcs = new List<Transform>();

	public Text dopeCounter;
	string basic = "Dopes: ";

	public delegate void DopeElminationHandler(bool noneLeft);
	public event DopeElminationHandler DopeEliminated;
	public List<Transform> NPCs
	{
		get { return npcs; }
	}

	private void Awake()
	{
		instance = this;
	}

	public void AddNPC(Transform t)
	{
		//Debug.Log(t.name);
		npcs.Add(t);
		UpdateCounter(npcs.Count);
	}

	private void UpdateCounter(int count)
	{
		dopeCounter.text = basic + count.ToString();
	}

	public void RemoveNPC(Transform t)
	{
		//this will stop the messages
		t.GetComponent<NPCHolder>().enabled = false;

		npcs.Remove(t);
		UpdateCounter(npcs.Count);
		if (DopeEliminated != null)
		{
			DopeEliminated(npcs.Count == 0);
		}
	}
}
