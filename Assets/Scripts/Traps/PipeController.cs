﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PipeController : MonoBehaviour
{
	public ParticleSystem topRight, topLeft,
		bottomRight,bottomLeft;
	Animator anim;
	public Collider topRightTrigger, bottomRightTrigger,
		topLeftTrigger, bottomLeftTrigger;
	//public delegate void SteamCollisionHandler(Collider other);
	//public event SteamCollisionHandler Collided;
	AudioSource source;
    // Start is called before the first frame update
    void Start()
    {
		anim = GetComponent<Animator>();
		source = GetComponent<AudioSource>();
		//pRightTrigger.
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnMouseDown()
	{
		//Debug.Log("hey");
		RaycastHit hit;
		Physics.Raycast(MessageSpawner.CurrentCamera.ScreenPointToRay(Input.mousePosition), out hit);
		Debug.DrawRay(MessageSpawner.CurrentCamera.transform.position,
			MessageSpawner.CurrentCamera.ScreenToWorldPoint(Input.mousePosition) - MessageSpawner.CurrentCamera.transform.position);
		Debug.Log(hit.collider.transform.name);
		if (hit.collider.transform.tag == "Burst")
		{
			//Debug.Log("yes");
			ParticleSystem ps = hit.collider.GetComponent<ParticleSystem>();

			if (ps == topRight)
			{
				anim.SetTrigger("TopRight");
			}

			if (ps == topLeft)
			{
				anim.SetTrigger("TopLeft");
			}

			if (ps == bottomRight)
			{
				anim.SetTrigger("BottomRight");
			}

			if (ps == bottomLeft)
			{
				anim.SetTrigger("BottomLeft");
			}

			ps.Play();
			source.Play();
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		//Debug.Log("hey");
		if (other.tag == "Dope")
		{
			//Debug.Log("dope");
			other.GetComponent<NavMeshAgent>().enabled = false;
			other.GetComponentInChildren<Animator>().enabled = false;
			//other.GetComponent<Rigidbody>().isKinematic = false;
			//other.GetComponent<Rigidbody>().useGravity = true;

			//GetComponent<Animator>().SetTrigger("Trigger");
			StartCoroutine(DelayedJump(other));
			//other.GetComponent<Rigidbody>().AddExplosionForce(1000f, transform.position, 10f);
		}
	}

	IEnumerator DelayedJump(Collider other)
	{
		float t;
		Vector3 target = transform.position;
		Vector2 temp;// = Random.insideUnitCircle;
					 //target.x += temp.x;
					 //target.z += temp.y;
					 //target.y = other.transform.position.y;

		int count = Random.Range(4, 7);
		Debug.Log(count);
		for (int i = 0; i < count; i++)
		{
			t = 0f;
			temp = Random.insideUnitCircle;
			target.x += temp.x;
			target.z += temp.y;
			target.y = other.transform.position.y;
			Quaternion rot = Quaternion.FromToRotation(other.transform.position, target);
			while (t < 0.25f)
			{
				t += Time.deltaTime;
				other.transform.rotation = Quaternion.Slerp(other.transform.rotation, rot, t / 0.25f);
				other.transform.position = Vector3.Lerp(other.transform.position, target, t / 0.25f);
				yield return null;
			}
		}

		//other.transform.Translate(transform.position);

		yield return new WaitForSeconds(.1f);
		//source.Play();

		other.GetComponent<Rigidbody>().AddForce(Vector3.up * 1000f);
		other.GetComponent<Rigidbody>().isKinematic = false;
		other.GetComponent<Rigidbody>().useGravity = true;

		foreach (var item in other.GetComponentsInChildren<Rigidbody>())
		{
			item.isKinematic = false;
			item.useGravity = true;
		}

		NPCRef.instance.RemoveNPC(other.transform);
	}
}
