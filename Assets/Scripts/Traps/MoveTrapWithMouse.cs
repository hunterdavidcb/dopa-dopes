﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveTrapWithMouse : MonoBehaviour
{
	Vector3 mousePos = Vector3.zero;
	bool posSet = false;
	bool isPit;
	
	public delegate void PlacementHandler(Vector3Int pos, GameObject go, bool isPit);
	public event PlacementHandler Placed;

	// Start is called before the first frame update
	void Start()
	{

	}

	public void SetIsPit(bool b)
	{
		isPit = b;
	}

	public bool PosSet
	{
		get { return posSet; }
	}

    // Update is called once per frame
    void Update()
    {
		RaycastHit hit;
		Ray ray = MessageSpawner.CurrentCamera.ScreenPointToRay(Input.mousePosition);
		//Debug.Log(ray);
		Vector3 temp = Vector3.zero;
		if (Physics.Raycast(ray, out hit))
		{
			temp = hit.point;
			if (temp != mousePos && !posSet)
			{
				temp.y = hit.collider.transform.position.y;
				temp.x = hit.collider.transform.position.x;
				temp.z = hit.collider.transform.position.z;
				mousePos = temp;
				OnMouseMoved(temp);
			}
		}
		//Debug.Log(temp);
		

		if (Input.GetMouseButtonDown(0))
		{
			OnMouseDown();
		}

		if (Input.GetKeyDown(KeyCode.R) && !Input.GetKeyDown(KeyCode.T))
		{
			transform.rotation = Quaternion.Euler(0f, 
				transform.rotation.eulerAngles.y - 90, 0f);
		}
		else if (!Input.GetKeyDown(KeyCode.R) && Input.GetKeyDown(KeyCode.T))
		{
			transform.rotation = Quaternion.Euler(0f,
				transform.rotation.eulerAngles.y + 90, 0f);
		}
    }

	protected void OnMouseMoved(Vector3 pos)
	{
		transform.position = pos;
	}

	private void OnMouseDown()
	{
		if (Placed != null)
		{
			Vector3Int pos = new Vector3Int((int)transform.position.x,
				(int)transform.position.y,
				(int)transform.position.z);
			Placed(pos, gameObject, isPit);

			if (GetComponent<NavMeshObstacle>())
			{
				GetComponent<NavMeshObstacle>().enabled = false;
			}
			else if (GetComponentInChildren<NavMeshObstacle>())
			{
				GetComponentInChildren<NavMeshObstacle>().enabled = false;
			}
		}
	}
}
