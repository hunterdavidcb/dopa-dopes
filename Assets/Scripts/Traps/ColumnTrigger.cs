﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ColumnTrigger : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		Debug.Log("hey");
		if (other.tag == "Dope")
		{
			Debug.Log("dope");
			other.GetComponent<NavMeshAgent>().enabled = false;
			other.GetComponent<Rigidbody>().isKinematic = false;
			other.GetComponent<Rigidbody>().useGravity = true;

			//GetComponent<Animator>().SetTrigger("Trigger");
			StartCoroutine(DelayedJump(other));
			//other.GetComponent<Rigidbody>().AddExplosionForce(1000f, transform.position, 10f);
		}
	}

	IEnumerator DelayedJump(Collider other)
	{
		float t = 0;
		RaycastHit hit;
		Physics.Raycast(other.transform.position, transform.position - other.transform.position, out hit);
		

		Vector3 target = hit.point;
		target.y = other.transform.position.y;
		while (t < 0.25f)
		{
			t += Time.deltaTime;
			other.transform.position = Vector3.Lerp(other.transform.position, target, t / 0.25f);
			yield return null;
		}
		//other.transform.Translate(transform.position);

		yield return new WaitForSeconds(.1f);
		//source.Play();

		other.GetComponent<Rigidbody>().AddForce(hit.normal * 100f);
		//
		NPCRef.instance.RemoveNPC(other.transform);
	}
}
