﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Water : MonoBehaviour
{
	[FMODUnity.EventRef]
	public string slip;
	//// Start is called before the first frame update
	//void Start()
	//{
	//	source = GetComponent<AudioSource>();
	//}

	private void OnTriggerEnter(Collider other)
	{
		//Debug.Log("hey");
		if (other.tag == "Dope")
		{
			Debug.Log("dope");
			other.GetComponent<NavMeshAgent>().enabled = false;
			other.GetComponentInChildren<Animator>().enabled = false;
			other.GetComponent<Rigidbody>().isKinematic = false;
			other.GetComponent<Rigidbody>().useGravity = true;

			foreach (var item in other.GetComponentsInChildren<Rigidbody>())
			{
				item.isKinematic = false;
				item.useGravity = true;
			}

			StartCoroutine(Slip(other));
			//other.GetComponent<Rigidbody>().AddExplosionForce(1000f, transform.position, 10f);
		}
	}

	IEnumerator Slip(Collider other)
	{
		float t = 0;
		Vector3 target = transform.position;
		target.y = other.transform.position.y;
		Vector3 dir = (target - other.transform.position).normalized;
		while (t < 0.25f)
		{
			t += Time.deltaTime;
			other.transform.position = Vector3.Lerp(other.transform.position, target, t / 0.25f);
			yield return null;
		}
		//other.transform.Translate(transform.position);

		yield return new WaitForSeconds(.1f);
		FMODUnity.RuntimeManager.PlayOneShot(slip, transform.position);

		other.GetComponent<Rigidbody>().AddForce(dir * 1000f);
		NPCRef.instance.RemoveNPC(other.transform);
	}
}
