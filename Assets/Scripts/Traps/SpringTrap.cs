﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpringTrap : MonoBehaviour
{
	[FMODUnity.EventRef]
	public string catapult;
	// Start is called before the first frame update
	//void Start()
	//{
	//	source = GetComponent<AudioSource>();
	//}

	//// Update is called once per frame
	//void Update()
	//{

	//}

	private void OnTriggerEnter(Collider other)
	{
		//Debug.Log("hey");
		if (other.tag == "Dope")
		{
			//Debug.Log("dope");
			other.GetComponent<NavMeshAgent>().enabled = false;
			other.GetComponentInChildren<Animator>().enabled = false;
			other.GetComponent<Rigidbody>().isKinematic = false;
			other.GetComponent<Rigidbody>().useGravity = true;

			foreach (var item in other.GetComponentsInChildren<Rigidbody>())
			{
				item.isKinematic = false;
				item.useGravity = true;
			}

			GetComponent<Animator>().SetTrigger("Trigger");
			StartCoroutine(DelayedJump(other));
			//other.GetComponent<Rigidbody>().AddExplosionForce(1000f, transform.position, 10f);
		}
	}

	IEnumerator DelayedJump(Collider other)
	{
		float t = 0;
		Vector3 target = transform.position;
		target.y = other.transform.position.y;
		while(t < 0.25f)
		{
			t += Time.deltaTime;
			other.transform.position = Vector3.Lerp(other.transform.position, target, t / 0.25f);
			yield return null;
		}
		
		yield return new WaitForSeconds(.1f);
		FMODUnity.RuntimeManager.PlayOneShot(catapult, transform.position);

		other.GetComponent<Rigidbody>().AddForce(Vector3.up * 1000f + Vector3.forward * 500f);
		NPCRef.instance.RemoveNPC(other.transform);
	}

}
