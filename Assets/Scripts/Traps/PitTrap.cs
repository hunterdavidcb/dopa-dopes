﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PitTrap : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		//Debug.Log("hey");
		if (other.tag == "Dope")
		{
			//Debug.Log("dope");
			other.GetComponent<NavMeshAgent>().enabled = false;
			other.GetComponentInChildren<Animator>().enabled = false;
			other.GetComponent<Rigidbody>().isKinematic = false;
			other.GetComponent<Rigidbody>().useGravity = true;

			foreach (var item in other.GetComponentsInChildren<Rigidbody>())
			{
				item.isKinematic = false;
				item.useGravity = true;
			}

			//GetComponent<Animator>().SetTrigger("Trigger");
			StartCoroutine(DelayedJump(other));
			//other.GetComponent<Rigidbody>().AddExplosionForce(1000f, transform.position, 10f);
		}
	}

	IEnumerator DelayedJump(Collider other)
	{
		float t = 0;
		Vector3 target = transform.position;
		target.y = other.transform.position.y;
		while (t < 0.25f)
		{
			t += Time.deltaTime;
			other.transform.position = Vector3.Lerp(other.transform.position, target, t / 0.25f);
			yield return null;
		}
		//other.transform.Translate(transform.position);

		yield return new WaitForSeconds(.1f);
		//source.Play();

		other.GetComponent<Rigidbody>().AddForce(-Vector3.up * 500f);
		NPCRef.instance.RemoveNPC(other.transform);
	}
}
