﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class SpawnTrapPreview : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public delegate void TooltipHandler(string text, Vector2 pos);
	public event TooltipHandler ShowTooltip;
	public event TooltipHandler HideTooltip;

	public string message;

	public Transform trap;
	public bool isPit;

	void Awake()
	{
		ShowTooltip st = FindObjectOfType<ShowTooltip>();
		ShowTooltip += st.OnShowTip;
		HideTooltip += st.OnHideTip;
	}

	// Start is called before the first frame update


	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void Spawn()
	{
		GameObject go = Instantiate(trap, Vector3.zero, Quaternion.identity).gameObject;
		MoveTrapWithMouse m = go.AddComponent<MoveTrapWithMouse>();
		m.SetIsPit(isPit);
		if (go.GetComponent<NavMeshObstacle>())
		{
			go.GetComponent<NavMeshObstacle>().enabled = false;
		}
		else if (go.GetComponentInChildren<NavMeshObstacle>())
		{
			go.GetComponentInChildren<NavMeshObstacle>().enabled = false;
		}
		

		m.Placed += FindObjectOfType<LevelGen>().OnTrapPlaced;
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		//Debug.Log("here first");
		if (ShowTooltip != null)
		{


			ShowTooltip(message,
				 new Vector2(transform.position.x + 180f, transform.position.y));
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (HideTooltip != null)
		{
			HideTooltip(null, Vector2.zero);
		}
	}
}
