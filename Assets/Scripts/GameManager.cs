﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public static GameManager instance;
	GameObject victoryPanel;
	LevelGen levelGenerator;

	int numCompleted = 0;

	public int NumberCompleted
	{
		get { return numCompleted; }
	}

	private void Awake()
	{
		instance = this;
		DontDestroyOnLoad(gameObject);
		SceneManager.LoadScene(1, LoadSceneMode.Additive);
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		if (scene.buildIndex == 1)
		{
			FindObjectOfType<Canvas>().transform.Find("Play").GetComponent<Button>().onClick.AddListener(LoadTrain);
		}
		else if (scene.buildIndex == 2)
		{
			levelGenerator = FindObjectOfType<LevelGen>();
			levelGenerator.Generate();
			victoryPanel = FindObjectOfType<Canvas>().transform.Find("VictoryPanel").gameObject;
			victoryPanel.GetComponent<VictoryPanel>().killMore.onClick.AddListener(KillMore);
			victoryPanel.GetComponent<VictoryPanel>().returnToMain.onClick.AddListener(ReturnToMain);
			victoryPanel.SetActive(false);
			NPCRef.instance.DopeEliminated += OnNCPEliminated;
		}
	}

	private void LoadTrain()
	{
		SceneManager.UnloadSceneAsync(1);
		SceneManager.LoadScene(2, LoadSceneMode.Additive);
	}

	public void OnNCPEliminated(bool cleared)
	{
		if (cleared)
		{
			// show victory screen
			numCompleted++;
			victoryPanel.SetActive(true);
		}
	}

	public void KillMore()
	{
		levelGenerator.Generate();
		victoryPanel.SetActive(false);
	}

	public void ReturnToMain()
	{
		SceneManager.UnloadSceneAsync(2);
		SceneManager.LoadScene(1, LoadSceneMode.Additive);
	}
}
