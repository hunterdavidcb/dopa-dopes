﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MakeMainCameraOnClick : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
	public delegate void TooltipHandler(string text, Vector2 pos);
	public event TooltipHandler ShowTooltip;
	public event TooltipHandler HideTooltip;

	public delegate void ClickHandler(Camera c);
	public event ClickHandler Clicked;

	public string message;

	Camera thisCamera;

	public void SetCamera(Camera c)
	{
		thisCamera = c;
	}

	void Awake()
	{
		ShowTooltip st = FindObjectOfType<ShowTooltip>();
		ShowTooltip += st.OnShowTip;
		HideTooltip += st.OnHideTip;
	}

	// Update is called once per frame
	void Update()
    {
        
    }


	public void OnPointerDown(PointerEventData eventData)
	{
		//Debug.Log("clicked " + name);
		if (Clicked != null)
		{
			Clicked(thisCamera);

		}
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
		//Debug.Log("here first");
		if (ShowTooltip != null)
		{


			ShowTooltip(message,
				 new Vector2(transform.position.x, transform.position.y + 160));
		}
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		if (HideTooltip != null)
		{
			HideTooltip(null, Vector2.zero);
		}
	}
}
