﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CameraSwitcher : MonoBehaviour
{ 

	List<Camera> cameras = new List<Camera>();
	Dictionary<Camera, RawImage> cameraImages = new Dictionary<Camera, RawImage>();
	Camera initialCamera;
	public Text levelText;

	//public RawImage ri;


	public delegate void CameraSwitchHandler(Camera currentCamera);
	public event CameraSwitchHandler CameraSwitched;

	 
	public void AddCamera(Camera c, RawImage ri)
	{
		cameras.Add(c);
		cameraImages.Add(c, ri);
	}

	public void RemoveCamera(Camera c)
	{
		cameras.Remove(c);
		Destroy(cameraImages[c].gameObject);
		cameraImages.Remove(c);
	}

	private void Awake()
	{
		initialCamera = Camera.main;
	}

	public void Reset()
	{
		initialCamera.enabled = true;
	}


	// Update is called once per frame
	void Update()
    {
        
    }

	public void RegisterCameraView(MakeMainCameraOnClick mmcc)
	{
		mmcc.Clicked += OnViewClicked;
	}

	protected void OnViewClicked(Camera c)
	{

		//RenderTexture rt = new RenderTexture(1024, 768, 16, RenderTextureFormat.ARGB32);
		//rt.Create();
		//c.targetTexture.Release();
		//ri.texture = c.targetTexture;
		//ri.texture ;
		

		for (int i = 0; i < cameras.Count; i++)
		{
			if (cameras[i] != c)
			{
				cameras[i].GetComponent<NPCVisibility>().enabled = false;
				//Debug.Log(cameras[i].name + " deactivated");
				RenderTexture newTexture = new RenderTexture(1024, 768, 16, RenderTextureFormat.ARGB32);
				newTexture.Create();
				cameras[i].targetTexture = newTexture;
				cameraImages[cameras[i]].texture = newTexture;
				cameras[i].GetComponent<FMODUnity.StudioListener>().enabled = false;
				cameras[i].GetComponent<CameraController>().SetMain(false);
			}
		}

		if (Camera.main.transform.parent == null)
		{
			Camera.main.enabled = false;
			SpawnTrapPreview[] stp = FindObjectsOfType<SpawnTrapPreview>();
			for (int i = 0; i < stp.Length; i++)
			{
				stp[i].GetComponent<Button>().interactable = true;
			}
		}

		c.targetTexture = null;
		c.transform.SetAsFirstSibling();
		c.GetComponent<NPCVisibility>().enabled = true;
		c.GetComponent<FMODUnity.StudioListener>().enabled = true;
		c.GetComponent<CameraController>().SetMain(true);
		
		  levelText.text = c.name;

		if (CameraSwitched != null)
		{
			CameraSwitched(c);
		}
	}
}
