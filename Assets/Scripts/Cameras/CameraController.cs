﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	//float maxH = 45f;
	//float minH = -45f;
	//float maxV = 20f;
	//float minV = -10f;

	Vector2 minRot = new Vector2(-45f, -10f);
	Vector2 maxRot = new Vector2(45f, 20f);

	float rotSpeed = 20f;

	Vector2 cumulativeRot = Vector2.zero;
	Vector2 baseRot;
	bool isMain = false;

	public void SetMain(bool b)
	{
		isMain = b;
	}
	
    // Start is called before the first frame update
    void Start()
    {
		baseRot = new Vector2(transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.x);
    }

    // Update is called once per frame
    void Update()
    {
		if ((Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) && isMain)
		{
			Vector2 tempRot = new Vector2(Input.GetAxis("Horizontal"), -Input.GetAxis("Vertical")) * rotSpeed * Time.deltaTime;

			if (tempRot.x + cumulativeRot.x < maxRot.x && tempRot.x + cumulativeRot.x > minRot.x)
			{
				cumulativeRot.x += tempRot.x;
			}

			if (tempRot.y + cumulativeRot.y < maxRot.y && tempRot.y + cumulativeRot.y > minRot.y)
			{
				cumulativeRot.y += tempRot.y;
			}

			transform.rotation = Quaternion.Euler(baseRot.y + cumulativeRot.y, baseRot.x + cumulativeRot.x, 0f);
		}
    }
}
