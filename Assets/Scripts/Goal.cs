﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
	public delegate void GoalDestructionHandler(Transform t);
	public event GoalDestructionHandler Destroyed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnDestroy()
	{
		if (Destroyed != null)
		{
			Destroyed(transform);
		}
	}
}
