﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalManager : MonoBehaviour
{
	// Start is called before the first frame update
	List<Transform> goals = new List<Transform>();
	//Dictionary<Vector3, Transform> npcAtGoal = new Dictionary<Vector3, Transform>();
	HashSet<Vector3> npcAtGoal = new HashSet<Vector3>();
	Dictionary<Vector3, Transform> npcGoingToGoal = new Dictionary<Vector3, Transform>();

	public List<Transform> Goals
	{
		get { return goals; }
	}

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void AddGoal(Transform g)
	{
		goals.Add(g);
		g.GetComponent<Goal>().Destroyed += RemoveGoal;
	}

	protected void RemoveGoal(Transform g)
	{
		goals.Remove(g);
	}

	public void RegisterNPC(NPCHolder npc)
	{
		npc.Arrived += SetNPCGoalOnArrival;
	}

	public bool SetNPCAtGoal(Transform t)
	{
		if (npcAtGoal.Contains(t.position))
		{
			return false;
		}
		else
		{
			npcAtGoal.Add(t.position);
			return true;
		}
	}

	public void UnRegisterNPC(NPCHolder npc)
	{
		npc.Arrived -= SetNPCGoalOnArrival;
	}

	protected void SetNPCGoalOnArrival(NPCHolder npc, Transform cGoal)
	{
		if (cGoal != null)
		{
			if (npcGoingToGoal.ContainsKey(cGoal.position))
			{
				if (npcGoingToGoal[cGoal.position] == npc)
				{
					npcGoingToGoal.Remove(cGoal.position);
				}
			}
		}
		
		
		Transform newGoal = goals[Random.Range(0, goals.Count)];

		while (newGoal == cGoal || npcGoingToGoal.ContainsKey(newGoal.position))
		{
			newGoal = goals[Random.Range(0, goals.Count)];
		}

		npc.SetGoal(newGoal);
	}
}
