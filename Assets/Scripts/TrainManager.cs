﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TrainManager : MonoBehaviour
{
	public Transform trainPrefab;

	Dictionary<Vector3Int, List<Train>> runningTrains = new Dictionary<Vector3Int, List<Train>>();
	Dictionary<Vector3Int, TrainTrack> trainTracks = new Dictionary<Vector3Int, TrainTrack>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void AddTrainTrack(Vector3Int beginning, Vector3Int end)
	{
		trainTracks.Add(beginning, new TrainTrack(beginning, end,
			Random.Range(20f, 60f), Random.Range(20f, 60f)));
		Debug.Log(trainTracks.Count);
		//Debug.Log("beginning: " + beginning);
		//Debug.Log("end: " + end);
	}

	public void RemoveTrainTrack(Vector3Int beginning)
	{
		trainTracks.Remove(beginning);
	}

	public void StartTrains()
	{
		StartCoroutine(HandleTrains());
	}

	IEnumerator HandleTrains()
	{
		while (true)
		{
			yield return new WaitForSeconds(1f);
			// grab a random track
			TrainTrack tt = trainTracks.ElementAt(Random.Range(0, trainTracks.Count)).Value;
			//spawn a train on it
			SpawnTrain(tt);

			yield return null;
		}
		
	}

	void SpawnTrain(TrainTrack tt)
	{
		if (runningTrains.ContainsKey(tt.beginning))
		{
			if (tt.secondsBetweenTrains <= Time.timeSinceLevelLoad -
				runningTrains[tt.beginning][runningTrains[tt.beginning].Count-1].lifeTime)
			{
				GameObject train = Instantiate(trainPrefab.gameObject);
				train.transform.position = tt.beginning;
				Vector2Int dir = new Vector2Int(tt.end.x - tt.beginning.x, tt.end.z - tt.beginning.z);
				if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
				{
					//Debug.Log("east-west");
					train.transform.rotation = Quaternion.Euler(0f, dir.x > 0 ? 90f : -90f, 0f);
				}
				else
				{
					//Debug.Log("north-south");
					train.transform.rotation = Quaternion.Euler(0f, dir.y > 0 ? 0f : 180f, 0f);
				}

				Train t;
				t.lifeTime = Time.timeSinceLevelLoad;
				t.t = train.transform;
				if (runningTrains[tt.beginning] == null)
				{
					List<Train> tList = new List<Train>();
					tList.Add(t);
					runningTrains[tt.beginning] = tList;
				}
				else
				{
					runningTrains[tt.beginning].Add(t);
				}

				StartCoroutine(MoveTrain(t.t, tt));
			}
		}
		else
		{
			GameObject train = Instantiate(trainPrefab.gameObject);
			train.transform.position = tt.beginning;
			Vector2Int dir = new Vector2Int(tt.end.x - tt.beginning.x, tt.end.z - tt.beginning.z);
			if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
			{
				//Debug.Log("east-west");
				train.transform.rotation = Quaternion.Euler(0f, dir.x > 0 ? 90f : -90f, 0f);
			}
			else
			{
				//Debug.Log("north-south");
				train.transform.rotation = Quaternion.Euler(0f, dir.y > 0 ? 0f : 180f, 0f);
			}
			Train t;
			t.lifeTime = Time.timeSinceLevelLoad;
			t.t = train.transform;

			List<Train> tList = new List<Train>();
			tList.Add(t);
			runningTrains.Add(tt.beginning, tList);

			StartCoroutine(MoveTrain(t.t, tt));
		}
		
	}

	IEnumerator MoveTrain(Transform t, TrainTrack tt)
	{
		Vector2Int dir = new Vector2Int(tt.end.x - tt.beginning.x, tt.end.z - tt.beginning.z);
		if (dir.x > 0 && dir.y == 0)
		{
			while (t.position.x < tt.end.x)
			{
				t.position = new Vector3(t.position.x + Time.deltaTime * tt.speed,
					t.position.y, t.position.z);
				yield return null;
			}
		}
		else if(dir.x < 0 && dir.y == 0)
		{
			while (t.position.x > tt.end.x)
			{
				t.position = new Vector3(t.position.x - Time.deltaTime * tt.speed,
					t.position.y, t.position.z);
				yield return null;
			}
		}
		else if (dir.x == 0 && dir.y > 0)
		{
			while (t.position.z < tt.end.z)
			{
				t.position = new Vector3(t.position.x,
					t.position.y, t.position.z + Time.deltaTime * tt.speed);
				yield return null;
			}
		}
		else if (dir.x == 0 && dir.y > 0)
		{
			while (t.position.z > tt.end.z)
			{
				t.position = new Vector3(t.position.x,
					t.position.y, t.position.z - Time.deltaTime * tt.speed);
				yield return null;
			}
		}

		runningTrains.Remove(tt.beginning);
		Destroy(t.gameObject);
	}

	struct TrainTrack
	{
		public Vector3Int beginning;
		public Vector3Int end;
		public float secondsBetweenTrains;
		public float speed;

		public TrainTrack(Vector3Int b, Vector3Int e, float t, float s)
		{
			beginning = b;
			end = e;
			secondsBetweenTrains = t;
			speed = s;
		}
	}

	struct Train
	{
		public Transform t;
		public float lifeTime;
	}
}
